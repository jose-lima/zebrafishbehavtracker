﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FlyCapture2Managed;
using System.IO;
using OpenTK;
using System.Globalization;
using System.Diagnostics;

namespace MicroScopeTracker1
{
    public unsafe partial class Form1 : Form
    {

        private bool doSaveData = false;
        private FileStream imageStream;
        private BinaryWriter imageWriter;
        private bool firstSave = true;
        private Stream myDataStream;
        private StreamWriter eyeDataStreamWriter;
        int selectClickRegion = 0;
        ManagedCamera m_camera = new ManagedCamera();
        private AutoResetEvent m_grabThreadExited;
        private BackgroundWorker m_grabThread;
        private bool m_grabImages;
        private bool doTracking=false;
        float fpi = (float)Math.PI;
        private int imageWidth, imageHeight;
        private ManagedImage m_rawImage;
        private ManagedImage m_processedImage;

        private int xScanTopVal,yScanTopVal,xScanBotVal,yScanBotVal;
        private int xLEyeVal,yLEyeVal,xREyeVal,yREyeVal;
        private int eyeThreshVal;
        private int xStartVal,yStartVal,xInitVal,yInitVal;

        public Form1()
        {
            InitializeComponent();
            loadDefaultParams();
        
            

        }
        private void loadDefaultParams()
        {
            try
            {
                StreamReader defaultParamsStreamReader = new StreamReader("defaultTrackingParams.prm");

                String caliString;
                String[] strParams;

                caliString = defaultParamsStreamReader.ReadLine();
                strParams = caliString.Split(' ');
                float[] fParams = new float[strParams.Length - 1];
                for (int ii = 0; ii < fParams.Length; ii++)
                {
                    fParams[ii] = Single.Parse(strParams[ii]);
                }
                setTrackingParameters(fParams);


                defaultParamsStreamReader.Close();
            }
            catch (Exception e3)
            {
                MessageBox.Show("No Default Parameters " + e3.ToString());
                // MessageBox.Show("No Default Parameters");
            }

            UpdateCaliValues();
        }
        private void startCameraButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (var manager = new ManagedBusManager())
                {
                    var guid = manager.GetCameraFromIndex(0);
                    try
                    {
                        m_camera.Connect(guid);
                    }
                    catch (Exception eee)
                    {
                        MessageBox.Show(eee.ToString());
                    }
                }


                CameraInfo camInfo = m_camera.GetCameraInfo();
                //UpdateFormCaption(camInfo);

                // Set embedded timestamp to on
                EmbeddedImageInfo embeddedInfo = m_camera.GetEmbeddedImageInfo();
                embeddedInfo.timestamp.onOff = true;
                m_camera.SetEmbeddedImageInfo(embeddedInfo);


                m_camera.StartCapture();

                m_grabImages = true;

                StartGrabLoop();
            }
            catch (FC2Exception ex)
            {
                //Debug.WriteLine("Failed to load form successfully: " + ex.Message);
                Environment.ExitCode = -1;
                Application.Exit();
                return;
            }
        }
        private void StartGrabLoop()
        {

            m_grabThread = new BackgroundWorker();
            m_grabThread.ProgressChanged += new ProgressChangedEventHandler(UpdateUI);
            m_grabThread.DoWork += new DoWorkEventHandler(GrabLoop);
            m_grabThread.WorkerReportsProgress = true;
            m_grabThread.RunWorkerAsync();
            m_grabThread.WorkerSupportsCancellation = true;
        }
        private void GrabLoop(object sender, DoWorkEventArgs e)
        {

            int[] scanLine;
            byte[] scanLineByte;
            int[] tailArc;
            long[] eyeMoments;

            double lEyeAngle, rEyeAngle;

            BackgroundWorker worker = sender as BackgroundWorker;
            m_rawImage = new ManagedImage();
            m_processedImage = new ManagedImage();
            int loopNumber = 0;
            Stopwatch myStopwatch = new Stopwatch();
            myStopwatch.Start();

            while (m_grabImages)
            {
                try
                {
                    m_camera.RetrieveBuffer(m_rawImage);
                }
                catch (FC2Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    //Debug.WriteLine("Error: " + ex.Message);
                    continue;
                }

                lock (this)
                {
                    if (loopNumber == 0)
                    {
                        imageWidth = (int)m_rawImage.cols;
                        imageHeight = (int)m_rawImage.rows;
                    }
                    if (doTracking)
                    {
                        //tail search parameters
                        int searchLength = 200; //previously 150
                        int numSteps = (int)Math.Ceiling(Math.PI * (double)searchLength);
                        byte[] saveValues = new byte[numSteps]; 
                        
                        scanLine = new int[yScanBotVal - yScanTopVal + 1];
                        scanLineByte = new byte[yScanBotVal - yScanTopVal + 1];
                       // lEyeAngle = 0;
                           //  rEyeAngle=0;
                        
                        //Track Eyes
                        eyeMoments = MikesUsefulFunctions.StackSafeFloodFillLessPlus2MomentsU8(m_rawImage.data, new System.Drawing.Point(imageWidth, imageHeight), new System.Drawing.Point(xLEyeVal, yLEyeVal), (byte)eyeThreshVal, (byte)255);
                        lEyeAngle = MikesUsefulFunctions.CalculatePrincipalAxisAngle(eyeMoments);
                        eyeMoments = MikesUsefulFunctions.StackSafeFloodFillLessPlus2MomentsU8(m_rawImage.data, new System.Drawing.Point(imageWidth, imageHeight), new System.Drawing.Point(xREyeVal, yREyeVal), (byte)eyeThreshVal, (byte)255);
                        rEyeAngle = MikesUsefulFunctions.CalculatePrincipalAxisAngle(eyeMoments);
                        //trackScan
                       
                        for (int ii = yScanTopVal; ii <= yScanBotVal; ii++)
                        {
                            scanLine[ii - yScanTopVal] = 0;
                            for (int jj = xScanTopVal; jj <= xScanBotVal; jj++)
                            {
                                scanLine[ii - yScanTopVal] += m_rawImage.data[jj+ii*imageWidth];
                            }
                            for (int jj = 0; jj < 5; jj++)
                            {
                                scanLineByte[ii - yScanTopVal] = (byte)(scanLine[ii - yScanTopVal] / (xScanBotVal - xScanTopVal + 1));
                                m_rawImage.data[ii - yScanTopVal + (imageHeight - 1 - jj) * imageWidth] = scanLineByte[ii - yScanTopVal] ;
                            }
                         }
                       
                        //Tail tracking
                        Vector2 startVector=new Vector2(xInitVal, yInitVal);
                        Vector2 normSearchVector;
                        Vector2 searchVector;
                        //arc length was 150 now 200 pixels
                        float stretchTop = 155.0f;
                        float stretchBot = 30.0f;
                        Vector2.Normalize(ref startVector,out normSearchVector);


                        float[] arcValues = new float[numSteps];
                        float[] nextValues = new float[numSteps];
                        int segwidth = 2;
                        for (int ii = -segwidth; ii <= segwidth; ii++)
                       {
                           Vector2.Multiply(ref normSearchVector,(float)(searchLength+ii), out searchVector);
                           nextValues = MikesUsefulFunctions.returnArcPointer(m_rawImage.data, new System.Drawing.Point(imageWidth, imageHeight), new Vector2(xStartVal, yStartVal), searchVector, fpi / 2.0f, numSteps);
                           for (int jj = 0; jj < numSteps; jj++)
                           {
                               arcValues[jj]+=nextValues[jj];
                           }
                         //  MessageBox.Show(nextValues[0].ToString());
                       }
                       
                   //  MessageBox.Show(arcValues[0].ToString());
                      for (int jj = 0; jj < numSteps; jj++)
                         {
                             saveValues[jj] = (byte)(arcValues[jj] / (float)((segwidth*2)+1));
                            // MessageBox.Show(arcValues[jj].ToString());
                             //arcValues[jj] -= stretchBot;
                           //  arcValues[jj] *= 255.0f/(stretchTop-stretchBot);
                             for (int kk = 15; kk <30; kk++)
                         {
                             m_rawImage.data[jj + (imageHeight - 1 - kk) * imageWidth] = saveValues[jj];
                         }
                         }


                     
                        //for (float thisAngle = -angle; thisAngle <= angle; thisAngle += 2 * angle / (float)Math.Ceiling(startVector.Length))
                         if (doSaveData)
                         {
                             if (firstSave)
                             {
                                 imageWriter.Write((int)(numSteps + scanLine.Length));
                                 //MessageBox.Show((numSteps + scanLine.Length).ToString());
                                 firstSave = false;
                             }
                             imageWriter.Write(saveValues);
                             // Clu
                            // MessageBox.Show(saveValues.Length.ToString());
                             imageWriter.Write(scanLineByte);
                            // MessageBox.Show(scanLineByte.Length.ToString());

                             eyeDataStreamWriter.Write(String.Concat(lEyeAngle.ToString(CultureInfo.GetCultureInfo("en-GB")), "\t"));
                             eyeDataStreamWriter.Write(String.Concat(rEyeAngle.ToString(CultureInfo.GetCultureInfo("en-GB")), "\t"));
                             eyeDataStreamWriter.Write(String.Concat((myStopwatch.ElapsedMilliseconds).ToString(CultureInfo.GetCultureInfo("en-GB")), "\t"));
                             eyeDataStreamWriter.Write("\n");

                         }

                    }

                    loopNumber++;
                    if (loopNumber % 20 == 0)
                    {
                        m_rawImage.Convert(PixelFormat.PixelFormatBgr, m_processedImage);
                        worker.ReportProgress(0);

                    }
                }
            }

            m_grabThreadExited.Set();
        }

        private void UpdateUI(object sender, ProgressChangedEventArgs e)
        {
            //   UpdateStatusBar();

            pictureBox1.Image = m_processedImage.bitmap;
            pictureBox1.Invalidate();
        }

        private void clickCalliButton_Click(object sender, EventArgs e)
        {
            selectClickRegion = 0;
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            int[] realLocation = screenToImage(new int[] { e.Location.X, e.Location.Y }, pictureBox1);
            switch (selectClickRegion % 6)
            {
                case 0:
                    xStart.Value = (decimal)realLocation[0];
                    yStart.Value = (decimal)realLocation[1];
                    break;
                case 1:
                    xInit.Value = (decimal)realLocation[0] - xStart.Value;
                    yInit.Value = (decimal)realLocation[1] - yStart.Value;
                    break;
                case 2:
                    xScanTop.Value = (decimal)realLocation[0];
                    yScanTop.Value = (decimal)realLocation[1];
                    break;
                case 3:
                    xScanBot.Value = (decimal)realLocation[0];
                    yScanBot.Value = (decimal)realLocation[1];
                    break;
                case 4:
                    xLEye.Value = (decimal)realLocation[0];
                    yLEye.Value = (decimal)realLocation[1];
                    break;
                case 5:
                    xREye.Value = (decimal)realLocation[0];
                    yREye.Value = (decimal)realLocation[1];
                    break;
                default:
                    break;
            }
            selectClickRegion++; 
        }
        private int[] screenToImage(int[] screenCoords, PictureBox myPictureBox)
        {
            double imageScale = Math.Min((double)myPictureBox.Size.Width / (double)myPictureBox.Image.Size.Width, (double)myPictureBox.Size.Height / (double)myPictureBox.Image.Size.Height);
            double scaledWidth = (double)myPictureBox.Image.Size.Width * imageScale;
            double scaledHeight = (double)myPictureBox.Image.Size.Height * imageScale;
            double imageX = ((double)myPictureBox.Size.Width - scaledWidth) / 2.0;
            double imageY = ((double)myPictureBox.Size.Height - scaledHeight) / 2.0;
            int[] scaledCoords = new int[2];
            scaledCoords[0] = (int)(((double)screenCoords[0] - imageX) / imageScale);
            scaledCoords[1] = (int)(((double)screenCoords[1] - imageY) / imageScale);
            return scaledCoords;
        }

        private void saveDefaultParams_Click(object sender, EventArgs e)
        {
            StreamWriter defaultParamStreamWriter = new StreamWriter("defaultTrackingParams.prm");
            float[] thisParams = getTrackingParameters();

            for (int ii = 0; ii < thisParams.Length; ii++)
            {
                defaultParamStreamWriter.Write("" + thisParams[ii].ToString() + " ");
            }
            defaultParamStreamWriter.WriteLine();
            defaultParamStreamWriter.Close();
        }
        private float[] getTrackingParameters()
        {
            float[] thisTrackParams = new float[13];
            thisTrackParams[0] = (float)xStart.Value;
            thisTrackParams[1] = (float)yStart.Value;
            thisTrackParams[2] = (float)xInit.Value;
            thisTrackParams[3] = (float)yInit.Value;
            thisTrackParams[4] = (float)xScanTop.Value;
            thisTrackParams[5] = (float)yScanTop.Value;
            thisTrackParams[6] = (float)xScanBot.Value;
            thisTrackParams[7] = (float)yScanBot.Value;
            thisTrackParams[8] = (float)xLEye.Value;
            thisTrackParams[9] = (float)yLEye.Value;
            thisTrackParams[10] = (float)xREye.Value;
            thisTrackParams[11] = (float)yREye.Value;
            thisTrackParams[12] = (float)eyeThreshold.Value;
            return thisTrackParams;
        }

        private void setTrackingParameters(float[] newTrackParams)
        {
            xStart.Value = (decimal)newTrackParams[0];
            yStart.Value = (decimal)newTrackParams[1];
            xInit.Value = (decimal)newTrackParams[2];
            yInit.Value = (decimal)newTrackParams[3];
            xScanTop.Value = (decimal)newTrackParams[4];
            yScanTop.Value = (decimal)newTrackParams[5];
            xScanBot.Value = (decimal)newTrackParams[6];
            yScanBot.Value = (decimal)newTrackParams[7];
            xLEye.Value = (decimal)newTrackParams[8];
            yLEye.Value = (decimal)newTrackParams[9];
            xREye.Value = (decimal)newTrackParams[10];
            yREye.Value = (decimal)newTrackParams[11];
            eyeThreshold.Value = (decimal)newTrackParams[12];
            return;
        }

        private void startTrackingButton_Click(object sender, EventArgs e)
        {
            doTracking = true;
        }
        private void UpdateCaliValues() {
            xScanTopVal=(int)xScanTop.Value;
            yScanTopVal=(int)yScanTop.Value;
            xScanBotVal=(int)xScanBot.Value;
            yScanBotVal=(int)yScanBot.Value;
            xLEyeVal=(int)xLEye.Value;
            yLEyeVal=(int)yLEye.Value;
            xREyeVal=(int)xREye.Value;
            yREyeVal=(int)yREye.Value;
            eyeThreshVal=(int)eyeThreshold.Value;
            xStartVal=(int)xStart.Value;
            yStartVal=(int)yStart.Value;
            xInitVal=(int)xInit.Value;
            yInitVal=(int)yInit.Value;
        }
        private void xStart_ValueChanged(object sender, EventArgs e)
        {
         UpdateCaliValues();
        }

        private void startSaveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            if (saveFileDialog1.FileName == "")
            {
                saveFileDialog1.Filter = "Text file|*.txt";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    
                    saveFileDialog1.FileName = saveFileDialog1.FileName.Replace(".txt", "eyes.txt");
                    if ((myDataStream = saveFileDialog1.OpenFile()) != null)
                    {
                        eyeDataStreamWriter = new StreamWriter(myDataStream);
                    }

                    imageStream = new FileStream(saveFileDialog1.FileName.Replace("eyes.txt", "scanandtail.dat"),FileMode.Create,FileAccess.Write);
                    imageWriter = new BinaryWriter(imageStream);

                    //FileStream imageStream2 = new FileStream(saveFileDialog1.FileName.Replace("eyes.txt", "scanandtail2.dat"), FileMode.Create, FileAccess.Write);
                   // BinaryWriter imageWriter2 = new BinaryWriter(imageStream);
                    //imageWriter2.Write((int) 577);
                    //imageWriter2.Write(new byte[] { 1,2,3,4,5});
                   // imageStream2.Close();
                }

            }


            doSaveData = true;
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            m_grabImages = false;
            Thread.Sleep(1000);
            try
            {
                imageStream.Close();
            } catch (Exception eeee) {

            }
            try
            {
                eyeDataStreamWriter.Close();
            }
            catch (Exception eeee)
            {

            }
        }

    }
}
