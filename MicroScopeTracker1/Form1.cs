﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FlyCapture2Managed;
using System.IO;
using OpenTK;
using System.Globalization;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;

namespace MicroScopeTracker1
{
    public unsafe partial class Form1 : Form
    {
        // Camera Point Grey
        ManagedBusManager manager;
        CameraInfo camInfo;
        ManagedPGRGuid guid;
        EmbeddedImageInfo embeddedInfo;
        ManagedCamera m_camera = new ManagedCamera();
        private ManagedImage m_rawImage;
        private ManagedImage m_processedImage;
        // Acquisition
        private int imageWidth, imageHeight;
        private BackgroundWorker m_grabThread;

        // Files
        private bool doSaveData = false;
        private bool firstSave = true;
        private StreamWriter eyeFileWriter, tailTimestampsWriter;
        BinaryWriter tailArchOneTwoLaserWriter;

        // GUI & display
        int selectClickRegion = 0;
        private static string[] CALIBSEQ = new string[]
        {
            "swim bladder","tail end", "top of laser det. ROI","bottom of laser det. ROI","left eye","right eye"
        };
        Stopwatch timerDisplay = Stopwatch.StartNew();
        long lastDispTime = 0;
        double displayFreq = Double.NaN;
        double acqFreq = Double.NaN;
        double tailFreq = Double.NaN;
        double eyeFreq = Double.NaN;
        long lastProcFrame = 0;

        //
        private bool doTracking = false;
        float fpi = (float)Math.PI;
                      
        public Form1()
        {
            InitializeComponent();
            loadDefaultParams();

            Tracker tracker = (Tracker)LoadObjFromXML(trackerFilePath, typeof(Tracker));
            TrackerToGUI(tracker);
        }

        private void TrackerToGUI(Tracker tracker)
        {
            this.textBox_savePath.Text = tracker.savePath;
            this.checkBox_saveData.Checked = tracker.doSaveData;
            this.checkBox_track.Checked = tracker.doTracking;
        }

        private void loadDefaultParams()
        {
            try
            {
                StreamReader defaultParamsStreamReader = new StreamReader("defaultTrackingParams.prm");

                String caliString;
                String[] strParams;

                caliString = defaultParamsStreamReader.ReadLine();
                strParams = caliString.Split(' ');
                float[] fParams = new float[strParams.Length - 1];
                for (int ii = 0; ii < fParams.Length; ii++)
                {
                    fParams[ii] = Single.Parse(strParams[ii]);
                }
                setTrackingParameters(fParams);


                defaultParamsStreamReader.Close();
            }
            catch (Exception e3)
            {
                MessageBox.Show("No Default Parameters " + e3.ToString());
                // MessageBox.Show("No Default Parameters");
            }

            UpdateCaliValues();
        }

        private void startCameraButton_Click(object sender, EventArgs e)
        {
            // Resume or restart saving?
            if (checkBox_saveData.Checked)
            {
                DialogResult dialogResult = MessageBox.Show("Restart acquisition?", "Start acquisition", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    CloseFiles();
                    OpenFiles(textBox_savePath.Text);
                }
            }

            //
            if (!StartAcquisition())
                return;

            StartGrabLoop();
            StartProcThreads();
        }

        private bool ConnectToCamera(int id = 0)
        {
            try
            {
                if (m_camera.IsConnected())
                {
                    MessageBox.Show("Camera is already connected.");
                    return true;
                }

                // Connect to cam
                manager = new ManagedBusManager();
                guid = manager.GetCameraFromIndex((uint)id);
                m_camera.Connect(guid);
                camInfo = m_camera.GetCameraInfo();
                PrintCamInfo(camInfo);

                // Set embedded timestamp to on
                embeddedInfo = m_camera.GetEmbeddedImageInfo();
                embeddedInfo.timestamp.onOff = true;
                embeddedInfo.frameCounter.onOff = true;
                m_camera.SetEmbeddedImageInfo(embeddedInfo);

                LogWriteLine("Camera is connected.");
                return true;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            return false;
        }

        private void LogWrite(string str)
        {
            textBox_log.AppendText(str + "\n\r");
        }

        private void LogWriteLine(string str)
        {
            LogWrite(str + "\n\r");
        }

        private void PrintCamInfo(CameraInfo camInfo)
        {
            var info = typeof(CameraInfo).GetProperties();
            foreach (var prop in info)
            {
                LogWriteLine(prop.Name + ": " + prop.GetValue(camInfo));
            }
        }

        private void UpdateUI(object sender, ProgressChangedEventArgs e)
        {
            // Update image
            pictureBox1.Image = m_processedImage.bitmap;
            pictureBox1.Invalidate();
            

            // Calculate frequencies      
            long elapsedFrames = (imgCopyEyes_Id - lastProcFrame);
            lastProcFrame = imgCopyEyes_Id;

            //Debug.WriteLine("Proc: " + inProcFrameId + " t:" + elapsedTime);
            if (ellapsedDisplayTime > 0)
            {
                displayFreq = Math.Round(1000.0 / ellapsedDisplayTime, 2);
                acqFreq = Math.Round(elapsedFrames * 1000.0 / ellapsedDisplayTime, 2);
                tailFreq = Math.Round(1000.0 / tailTime * 3350, 2);
                eyeFreq = Math.Round(1000.0 / eyeTime * 3350, 2);
               

                // UpdateStatusBar 
                statusFrame.Text = imgCopyEyes_Id.ToString();
                statusFreq.Text = String.Format("{0:000.00}/{1:000.00}/{2:000.00}/{3:000.00} queue: {4}",
                    acqFreq.ToString("000"),
                    tailFreq.ToString("0000"),
                    eyeFreq.ToString("000000"),
                    displayFreq.ToString("000"),
                    tailImgQueue.Count);
                
            }
          
        }


        private void clickCalliButton_Click(object sender, EventArgs e)
        {
            selectClickRegion = 0;
            statusInfo.Text = "Tracking calib: pick " + CALIBSEQ[selectClickRegion] + "...";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StopTracking();
            StopCapture();
            FlushFiles();
        }

        private void StopCapture()
        {
            if (m_grabThread != null && m_grabThread.IsBusy)
                m_grabThread.CancelAsync();
        }

        private void StopTracking()
        {
            if (tailThread != null && tailThread.IsBusy)
                tailThread.CancelAsync();
        }

        private void FlushFiles()
        {
            if (eyeFileWriter != null)
            {
                eyeFileWriter.Flush();
                tailArchOneTwoLaserWriter.Flush();
                tailTimestampsWriter.Flush();
            }
        }

        private void checkBox_track_CheckedChanged(object sender, EventArgs e)
        {
            doTracking = ((CheckBox)sender).Checked;
        }

        private void checkBox_saveData_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked) // turning on
            {
                // fill path if empty
                if (textBox_savePath.Text.Length <= 0)
                {
                    // fill path or exit
                    if (!SelectSavePath())
                    {
                        ((CheckBox)sender).Checked = (doSaveData = false);
                        return;
                    }
                }

                // open file writers if null                         
                if (eyeFileWriter == null)
                {
                    if (!OpenFiles(textBox_savePath.Text))
                    {
                        ((CheckBox)sender).Checked = (doSaveData = false);
                        return;
                    }
                }
            }

            // finally
            doSaveData = ((CheckBox)sender).Checked;

        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {

            int[] realLocation = screenToImage(new int[] { e.Location.X, e.Location.Y }, pictureBox1);
            switch (selectClickRegion % 6)
            {
                case 0:
                    xStart.Value = (decimal)realLocation[0]; // error: when closing program without stopping things.
                    yStart.Value = (decimal)realLocation[1];
                    break;
                case 1:
                    xInit.Value = (decimal)realLocation[0] - xStart.Value;
                    yInit.Value = (decimal)realLocation[1] - yStart.Value;
                    break;
                case 2:
                    xScanTop.Value = (decimal)realLocation[0];
                    yScanTop.Value = (decimal)realLocation[1];
                    break;
                case 3:
                    xScanBot.Value = (decimal)realLocation[0];
                    yScanBot.Value = (decimal)realLocation[1];
                    break;
                case 4:
                    xLEye.Value = (decimal)realLocation[0];
                    yLEye.Value = (decimal)realLocation[1];
                    break;
                case 5:
                    xREye.Value = (decimal)realLocation[0];
                    yREye.Value = (decimal)realLocation[1];
                    break;
                default:
                    break;
            }
            selectClickRegion++;
            if (selectClickRegion < CALIBSEQ.Length)
                statusInfo.Text = "Tracking calib: pick " + CALIBSEQ[selectClickRegion] + "...";
            else
                statusInfo.Text = "Tracking calib: completed.";
        }

        private int[] screenToImage(int[] screenCoords, PictureBox myPictureBox)
        {
            double imageScale = Math.Min((double)myPictureBox.Size.Width / (double)myPictureBox.Image.Size.Width, (double)myPictureBox.Size.Height / (double)myPictureBox.Image.Size.Height);
            double scaledWidth = (double)myPictureBox.Image.Size.Width * imageScale;
            double scaledHeight = (double)myPictureBox.Image.Size.Height * imageScale;
            double imageX = ((double)myPictureBox.Size.Width - scaledWidth) / 2.0;
            double imageY = ((double)myPictureBox.Size.Height - scaledHeight) / 2.0;
            int[] scaledCoords = new int[2];
            scaledCoords[0] = (int)(((double)screenCoords[0] - imageX) / imageScale);
            scaledCoords[1] = (int)(((double)screenCoords[1] - imageY) / imageScale);
            return scaledCoords;
        }

        private void saveDefaultParams_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }

        private void SaveSettings()
        {
            StreamWriter defaultParamStreamWriter = new StreamWriter("defaultTrackingParams.prm");
            float[] thisParams = getTrackingParameters();

            for (int ii = 0; ii < thisParams.Length; ii++)
            {
                defaultParamStreamWriter.Write("" + thisParams[ii].ToString() + " ");
            }
            defaultParamStreamWriter.WriteLine();
            defaultParamStreamWriter.Close();

            // Serialize tracker
            Tracker tracker = GetTrackerFromGUI();
            SaveObjToXML(trackerFilePath, tracker, typeof(Tracker));
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        }

        private bool SaveObjToXML(string filename, object obj, Type type)
        {
            XmlSerializer ser = new XmlSerializer(type);
            try
            {
                TextWriter writer = new StreamWriter(filename);
                ser.Serialize(writer, obj);
                writer.Close();
                return true;
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
            }
            return false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConnectToCamera();

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            ConnectToCamera();
        }

        private object LoadObjFromXML(string filename, Type type)
        {
            XmlSerializer ser = new XmlSerializer(type);
            object obj = null;
            try
            {
                StreamReader reader = new StreamReader(filename);
                obj = ser.Deserialize(reader);
                reader.Close();
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
            }
            return obj;
        }

        private Tracker GetTrackerFromGUI()
        {
            Tracker tracker = new Tracker();
            tracker.doSaveData = this.doSaveData;
            tracker.doTracking = this.doTracking;
            tracker.savePath = this.textBox_savePath.Text;
            return tracker;
        }

        private float[] getTrackingParameters()
        {
            float[] thisTrackParams = new float[13];
            thisTrackParams[0] = (float)xStart.Value;
            thisTrackParams[1] = (float)yStart.Value;
            thisTrackParams[2] = (float)xInit.Value;
            thisTrackParams[3] = (float)yInit.Value;
            thisTrackParams[4] = (float)xScanTop.Value;
            thisTrackParams[5] = (float)yScanTop.Value;
            thisTrackParams[6] = (float)xScanBot.Value;
            thisTrackParams[7] = (float)yScanBot.Value;
            thisTrackParams[8] = (float)xLEye.Value;
            thisTrackParams[9] = (float)yLEye.Value;
            thisTrackParams[10] = (float)xREye.Value;
            thisTrackParams[11] = (float)yREye.Value;
            thisTrackParams[12] = (float)eyeThreshold.Value;
            return thisTrackParams;
        }

        private void setTrackingParameters(float[] newTrackParams)
        {
            xStart.Value = (decimal)newTrackParams[0];
            yStart.Value = (decimal)newTrackParams[1];
            xInit.Value = (decimal)newTrackParams[2];
            yInit.Value = (decimal)newTrackParams[3];
            xScanTop.Value = (decimal)newTrackParams[4];
            yScanTop.Value = (decimal)newTrackParams[5];
            xScanBot.Value = (decimal)newTrackParams[6];
            yScanBot.Value = (decimal)newTrackParams[7];
            xLEye.Value = (decimal)newTrackParams[8];
            yLEye.Value = (decimal)newTrackParams[9];
            xREye.Value = (decimal)newTrackParams[10];
            yREye.Value = (decimal)newTrackParams[11];
            eyeThreshold.Value = (decimal)newTrackParams[12];
            return;
        }

        private void startTrackingButton_Click(object sender, EventArgs e)
        {
            doTracking = true;
        }

        private void UpdateCaliValues()
        {
            xScanTopVal = (int)xScanTop.Value;
            yScanTopVal = (int)yScanTop.Value;
            xScanBotVal = (int)xScanBot.Value;
            yScanBotVal = (int)yScanBot.Value;
            xLEyeVal = (int)xLEye.Value;
            yLEyeVal = (int)yLEye.Value;
            xREyeVal = (int)xREye.Value;
            yREyeVal = (int)yREye.Value;
            eyeThreshVal = (int)eyeThreshold.Value;
            xStartVal = (int)xStart.Value;
            yStartVal = (int)yStart.Value;
            xInitVal = (int)xInit.Value;
            yInitVal = (int)yInit.Value;
        }

        private void xStart_ValueChanged(object sender, EventArgs e)
        {
            UpdateCaliValues();
        }

        private bool SelectSavePath()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            if (saveFileDialog1.FileName == "")
            {
                saveFileDialog1.Filter = "Text file|*.txt";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    textBox_savePath.Text = saveFileDialog1.FileName;

                    return true;
                }

            }
            return false;
        }

        private void startSaveButton_Click(object sender, EventArgs e)
        {
            SelectSavePath();
        }
       
        private bool OpenFiles(string path)
        {
            

            try
            {
                // eyes
                string filename = path.Replace(".txt", "eyes.txt");
                eyeFileWriter = new StreamWriter(filename);
                eyeFileWriter.Write("lEyeAngle\t");
                eyeFileWriter.Write("rEyeAngle\t");
                eyeFileWriter.Write("timeOfLogWriteMs\t");
                eyeFileWriter.Write("metaTimestampSec\t");
                eyeFileWriter.Write("metaTimestampUs\t");
                eyeFileWriter.Write("metaFrameCounter\t");
                eyeFileWriter.WriteLine();

                // tail timestamps
                filename = path.Replace(".txt", "tailTimestamps.txt");
                tailTimestampsWriter = new StreamWriter(filename);
                tailTimestampsWriter.Write("metaTimestampSec\t");
                tailTimestampsWriter.Write("metaTimestampUs\t");
                tailTimestampsWriter.Write("metaFrameCounter\t");
                tailTimestampsWriter.WriteLine();
                // tail arch 1 + arch 2 + laser
                filename = path.Replace(".txt", "tailArchOneTwoLaser.dat");
                tailArchOneTwoLaserWriter = new BinaryWriter(new FileStream(filename, FileMode.Create, FileAccess.Write));
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
                CloseFiles();
                return false;
            }
            return true;
        }

        private void CloseFiles()
        {
            try
            {
                tailArchOneTwoLaserWriter.Close();
                eyeFileWriter.Close();
                tailTimestampsWriter.Close();
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
            }
        }
    }
}
