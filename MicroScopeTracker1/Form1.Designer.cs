﻿namespace MicroScopeTracker1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_startCam = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.eyeThresholdLabel = new System.Windows.Forms.Label();
            this.eyeThreshold = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.yREye = new System.Windows.Forms.NumericUpDown();
            this.xREye = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.yLEye = new System.Windows.Forms.NumericUpDown();
            this.xLEye = new System.Windows.Forms.NumericUpDown();
            this.clickCaliButton = new System.Windows.Forms.Button();
            this.yInitLabel = new System.Windows.Forms.Label();
            this.xInitLabel = new System.Windows.Forms.Label();
            this.yInit = new System.Windows.Forms.NumericUpDown();
            this.xInit = new System.Windows.Forms.NumericUpDown();
            this.yStartLabel = new System.Windows.Forms.Label();
            this.xStartLabel = new System.Windows.Forms.Label();
            this.yStart = new System.Windows.Forms.NumericUpDown();
            this.xStart = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.yScanBot = new System.Windows.Forms.NumericUpDown();
            this.xScanBot = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.yScanTop = new System.Windows.Forms.NumericUpDown();
            this.xScanTop = new System.Windows.Forms.NumericUpDown();
            this.startSaveButton = new System.Windows.Forms.Button();
            this.button_stopCam = new System.Windows.Forms.Button();
            this.checkBox_track = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusFrame = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusFreq = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.textBox_savePath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox_saveData = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_log = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eyeThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yREye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xREye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yLEye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xLEye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yScanBot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xScanBot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yScanTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xScanTop)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_startCam
            // 
            this.button_startCam.Location = new System.Drawing.Point(138, 12);
            this.button_startCam.Name = "button_startCam";
            this.button_startCam.Size = new System.Drawing.Size(92, 23);
            this.button_startCam.TabIndex = 0;
            this.button_startCam.Text = "Start Camera";
            this.button_startCam.UseVisualStyleBackColor = true;
            this.button_startCam.Click += new System.EventHandler(this.startCameraButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.LightGray;
            this.pictureBox1.Location = new System.Drawing.Point(425, 12);
            this.pictureBox1.MinimumSize = new System.Drawing.Size(640, 512);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(640, 512);
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            // 
            // eyeThresholdLabel
            // 
            this.eyeThresholdLabel.AutoSize = true;
            this.eyeThresholdLabel.Location = new System.Drawing.Point(14, 24);
            this.eyeThresholdLabel.Name = "eyeThresholdLabel";
            this.eyeThresholdLabel.Size = new System.Drawing.Size(75, 13);
            this.eyeThresholdLabel.TabIndex = 54;
            this.eyeThresholdLabel.Text = "Eye Threshold";
            // 
            // eyeThreshold
            // 
            this.eyeThreshold.Location = new System.Drawing.Point(95, 22);
            this.eyeThreshold.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.eyeThreshold.Name = "eyeThreshold";
            this.eyeThreshold.Size = new System.Drawing.Size(94, 20);
            this.eyeThreshold.TabIndex = 53;
            this.eyeThreshold.Value = new decimal(new int[] {
            107,
            0,
            0,
            0});
            this.eyeThreshold.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 52;
            this.label9.Text = "R Eye Y";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label10.Location = new System.Drawing.Point(14, 104);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 51;
            this.label10.Text = "R Eye X";
            // 
            // yREye
            // 
            this.yREye.Location = new System.Drawing.Point(95, 129);
            this.yREye.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yREye.Name = "yREye";
            this.yREye.Size = new System.Drawing.Size(94, 20);
            this.yREye.TabIndex = 50;
            this.yREye.Value = new decimal(new int[] {
            199,
            0,
            0,
            0});
            this.yREye.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xREye
            // 
            this.xREye.Location = new System.Drawing.Point(95, 102);
            this.xREye.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xREye.Name = "xREye";
            this.xREye.Size = new System.Drawing.Size(94, 20);
            this.xREye.TabIndex = 49;
            this.xREye.Value = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.xREye.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 48;
            this.label7.Text = "L Eye Y";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label8.Location = new System.Drawing.Point(14, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 47;
            this.label8.Text = "L Eye X";
            // 
            // yLEye
            // 
            this.yLEye.Location = new System.Drawing.Point(94, 76);
            this.yLEye.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yLEye.Name = "yLEye";
            this.yLEye.Size = new System.Drawing.Size(94, 20);
            this.yLEye.TabIndex = 46;
            this.yLEye.Value = new decimal(new int[] {
            199,
            0,
            0,
            0});
            this.yLEye.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xLEye
            // 
            this.xLEye.Location = new System.Drawing.Point(94, 50);
            this.xLEye.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xLEye.Name = "xLEye";
            this.xLEye.Size = new System.Drawing.Size(94, 20);
            this.xLEye.TabIndex = 45;
            this.xLEye.Value = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.xLEye.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // clickCaliButton
            // 
            this.clickCaliButton.Location = new System.Drawing.Point(218, 220);
            this.clickCaliButton.Name = "clickCaliButton";
            this.clickCaliButton.Size = new System.Drawing.Size(110, 24);
            this.clickCaliButton.TabIndex = 55;
            this.clickCaliButton.Text = "Calibrate tracking";
            this.clickCaliButton.UseVisualStyleBackColor = true;
            this.clickCaliButton.Click += new System.EventHandler(this.clickCalliButton_Click);
            // 
            // yInitLabel
            // 
            this.yInitLabel.AutoSize = true;
            this.yInitLabel.Location = new System.Drawing.Point(19, 125);
            this.yInitLabel.Name = "yInitLabel";
            this.yInitLabel.Size = new System.Drawing.Size(51, 13);
            this.yInitLabel.TabIndex = 63;
            this.yInitLabel.Text = "Y Search";
            // 
            // xInitLabel
            // 
            this.xInitLabel.AutoSize = true;
            this.xInitLabel.Location = new System.Drawing.Point(17, 99);
            this.xInitLabel.Name = "xInitLabel";
            this.xInitLabel.Size = new System.Drawing.Size(51, 13);
            this.xInitLabel.TabIndex = 62;
            this.xInitLabel.Text = "X Search";
            // 
            // yInit
            // 
            this.yInit.Location = new System.Drawing.Point(91, 118);
            this.yInit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yInit.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.yInit.Name = "yInit";
            this.yInit.Size = new System.Drawing.Size(94, 20);
            this.yInit.TabIndex = 61;
            this.yInit.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.yInit.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xInit
            // 
            this.xInit.Location = new System.Drawing.Point(91, 92);
            this.xInit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xInit.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.xInit.Name = "xInit";
            this.xInit.Size = new System.Drawing.Size(94, 20);
            this.xInit.TabIndex = 60;
            this.xInit.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // yStartLabel
            // 
            this.yStartLabel.AutoSize = true;
            this.yStartLabel.Location = new System.Drawing.Point(22, 73);
            this.yStartLabel.Name = "yStartLabel";
            this.yStartLabel.Size = new System.Drawing.Size(39, 13);
            this.yStartLabel.TabIndex = 59;
            this.yStartLabel.Text = "Y Start";
            // 
            // xStartLabel
            // 
            this.xStartLabel.AutoSize = true;
            this.xStartLabel.Location = new System.Drawing.Point(22, 47);
            this.xStartLabel.Name = "xStartLabel";
            this.xStartLabel.Size = new System.Drawing.Size(39, 13);
            this.xStartLabel.TabIndex = 58;
            this.xStartLabel.Text = "X Start";
            // 
            // yStart
            // 
            this.yStart.Location = new System.Drawing.Point(91, 66);
            this.yStart.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yStart.Name = "yStart";
            this.yStart.Size = new System.Drawing.Size(94, 20);
            this.yStart.TabIndex = 57;
            this.yStart.Value = new decimal(new int[] {
            199,
            0,
            0,
            0});
            this.yStart.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xStart
            // 
            this.xStart.Location = new System.Drawing.Point(91, 40);
            this.xStart.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xStart.Name = "xStart";
            this.xStart.Size = new System.Drawing.Size(94, 20);
            this.xStart.TabIndex = 56;
            this.xStart.Value = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.xStart.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 238);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 71;
            this.label1.Text = "Scan BR y";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 212);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 70;
            this.label2.Text = "Scan BR x";
            // 
            // yScanBot
            // 
            this.yScanBot.Location = new System.Drawing.Point(91, 231);
            this.yScanBot.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yScanBot.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.yScanBot.Name = "yScanBot";
            this.yScanBot.Size = new System.Drawing.Size(94, 20);
            this.yScanBot.TabIndex = 69;
            this.yScanBot.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.yScanBot.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xScanBot
            // 
            this.xScanBot.Location = new System.Drawing.Point(91, 205);
            this.xScanBot.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xScanBot.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.xScanBot.Name = "xScanBot";
            this.xScanBot.Size = new System.Drawing.Size(94, 20);
            this.xScanBot.TabIndex = 68;
            this.xScanBot.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 67;
            this.label3.Text = "Scan TL y";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 66;
            this.label4.Text = "Scan TL x";
            // 
            // yScanTop
            // 
            this.yScanTop.Location = new System.Drawing.Point(91, 179);
            this.yScanTop.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yScanTop.Name = "yScanTop";
            this.yScanTop.Size = new System.Drawing.Size(94, 20);
            this.yScanTop.TabIndex = 65;
            this.yScanTop.Value = new decimal(new int[] {
            199,
            0,
            0,
            0});
            this.yScanTop.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xScanTop
            // 
            this.xScanTop.Location = new System.Drawing.Point(91, 153);
            this.xScanTop.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xScanTop.Name = "xScanTop";
            this.xScanTop.Size = new System.Drawing.Size(94, 20);
            this.xScanTop.TabIndex = 64;
            this.xScanTop.Value = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.xScanTop.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // startSaveButton
            // 
            this.startSaveButton.Location = new System.Drawing.Point(306, 311);
            this.startSaveButton.Name = "startSaveButton";
            this.startSaveButton.Size = new System.Drawing.Size(31, 20);
            this.startSaveButton.TabIndex = 74;
            this.startSaveButton.Text = "...";
            this.startSaveButton.UseVisualStyleBackColor = true;
            this.startSaveButton.Click += new System.EventHandler(this.startSaveButton_Click);
            // 
            // button_stopCam
            // 
            this.button_stopCam.Location = new System.Drawing.Point(235, 12);
            this.button_stopCam.Name = "button_stopCam";
            this.button_stopCam.Size = new System.Drawing.Size(92, 23);
            this.button_stopCam.TabIndex = 76;
            this.button_stopCam.Text = "Stop Camera";
            this.button_stopCam.UseVisualStyleBackColor = true;
            this.button_stopCam.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox_track
            // 
            this.checkBox_track.AutoSize = true;
            this.checkBox_track.Location = new System.Drawing.Point(22, 19);
            this.checkBox_track.Name = "checkBox_track";
            this.checkBox_track.Size = new System.Drawing.Size(54, 17);
            this.checkBox_track.TabIndex = 77;
            this.checkBox_track.Text = "Track";
            this.checkBox_track.UseVisualStyleBackColor = true;
            this.checkBox_track.CheckedChanged += new System.EventHandler(this.checkBox_track_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.xStart);
            this.groupBox1.Controls.Add(this.checkBox_track);
            this.groupBox1.Controls.Add(this.yStart);
            this.groupBox1.Controls.Add(this.xStartLabel);
            this.groupBox1.Controls.Add(this.yStartLabel);
            this.groupBox1.Controls.Add(this.xInit);
            this.groupBox1.Controls.Add(this.yInit);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.xInitLabel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.yInitLabel);
            this.groupBox1.Controls.Add(this.yScanBot);
            this.groupBox1.Controls.Add(this.xScanTop);
            this.groupBox1.Controls.Add(this.xScanBot);
            this.groupBox1.Controls.Add(this.yScanTop);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 263);
            this.groupBox1.TabIndex = 78;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tail tracking";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.eyeThresholdLabel);
            this.groupBox2.Controls.Add(this.xLEye);
            this.groupBox2.Controls.Add(this.yLEye);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.xREye);
            this.groupBox2.Controls.Add(this.yREye);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.eyeThreshold);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(218, 44);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 170);
            this.groupBox2.TabIndex = 79;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Eye tracking";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.statusFrame,
            this.toolStripStatusLabel2,
            this.statusFreq,
            this.toolStripStatusLabel3,
            this.statusInfo});
            this.statusStrip1.Location = new System.Drawing.Point(0, 532);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(868, 22);
            this.statusStrip1.TabIndex = 80;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(97, 17);
            this.toolStripStatusLabel1.Text = "Processed frame:";
            // 
            // statusFrame
            // 
            this.statusFrame.Name = "statusFrame";
            this.statusFrame.Size = new System.Drawing.Size(12, 17);
            this.statusFrame.Text = "-";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(139, 17);
            this.toolStripStatusLabel2.Text = "Freq (acq/tail/eyes/disp):";
            // 
            // statusFreq
            // 
            this.statusFreq.Name = "statusFreq";
            this.statusFreq.Size = new System.Drawing.Size(12, 17);
            this.statusFreq.Text = "-";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(31, 17);
            this.toolStripStatusLabel3.Text = "Info:";
            // 
            // statusInfo
            // 
            this.statusInfo.Name = "statusInfo";
            this.statusInfo.Size = new System.Drawing.Size(12, 17);
            this.statusInfo.Text = "-";
            // 
            // textBox_savePath
            // 
            this.textBox_savePath.Location = new System.Drawing.Point(74, 311);
            this.textBox_savePath.Name = "textBox_savePath";
            this.textBox_savePath.Size = new System.Drawing.Size(223, 20);
            this.textBox_savePath.TabIndex = 81;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 314);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 82;
            this.label5.Text = "Save path";
            // 
            // checkBox_saveData
            // 
            this.checkBox_saveData.AutoSize = true;
            this.checkBox_saveData.Location = new System.Drawing.Point(343, 313);
            this.checkBox_saveData.Name = "checkBox_saveData";
            this.checkBox_saveData.Size = new System.Drawing.Size(75, 17);
            this.checkBox_saveData.TabIndex = 83;
            this.checkBox_saveData.Text = "Save data";
            this.checkBox_saveData.UseVisualStyleBackColor = true;
            this.checkBox_saveData.CheckedChanged += new System.EventHandler(this.checkBox_saveData_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 23);
            this.button2.TabIndex = 84;
            this.button2.Text = "Connect to camera";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 339);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 85;
            this.label6.Text = "Log:";
            // 
            // textBox_log
            // 
            this.textBox_log.BackColor = System.Drawing.SystemColors.Control;
            this.textBox_log.ForeColor = System.Drawing.Color.Gray;
            this.textBox_log.Location = new System.Drawing.Point(12, 355);
            this.textBox_log.Multiline = true;
            this.textBox_log.Name = "textBox_log";
            this.textBox_log.ReadOnly = true;
            this.textBox_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_log.Size = new System.Drawing.Size(406, 169);
            this.textBox_log.TabIndex = 86;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(868, 554);
            this.Controls.Add(this.textBox_log);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.checkBox_saveData);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox_savePath);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button_stopCam);
            this.Controls.Add(this.startSaveButton);
            this.Controls.Add(this.clickCaliButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button_startCam);
            this.Name = "Form1";
            this.Text = "Fish Tracker (Point Grey / Laser detect / Eye track / Tail with 2 arches)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eyeThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yREye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xREye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yLEye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xLEye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yScanBot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xScanBot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yScanTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xScanTop)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_startCam;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label eyeThresholdLabel;
        private System.Windows.Forms.NumericUpDown eyeThreshold;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown yREye;
        private System.Windows.Forms.NumericUpDown xREye;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown yLEye;
        private System.Windows.Forms.NumericUpDown xLEye;
        private System.Windows.Forms.Button clickCaliButton;
        private System.Windows.Forms.Label yInitLabel;
        private System.Windows.Forms.Label xInitLabel;
        private System.Windows.Forms.NumericUpDown yInit;
        private System.Windows.Forms.NumericUpDown xInit;
        private System.Windows.Forms.Label yStartLabel;
        private System.Windows.Forms.Label xStartLabel;
        private System.Windows.Forms.NumericUpDown yStart;
        private System.Windows.Forms.NumericUpDown xStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown yScanBot;
        private System.Windows.Forms.NumericUpDown xScanBot;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown yScanTop;
        private System.Windows.Forms.NumericUpDown xScanTop;
        private System.Windows.Forms.Button startSaveButton;
        private System.Windows.Forms.Button button_stopCam;
        private System.Windows.Forms.CheckBox checkBox_track;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel statusFrame;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel statusFreq;
        private System.Windows.Forms.TextBox textBox_savePath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel statusInfo;
        private System.Windows.Forms.CheckBox checkBox_saveData;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_log;
    }
}

