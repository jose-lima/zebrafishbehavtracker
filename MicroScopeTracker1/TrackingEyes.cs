﻿using FlyCapture2Managed;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MicroScopeTracker1
{
    public unsafe partial class Form1 : Form
    {

        // tracking parameters
        private int xLEyeVal, yLEyeVal, xREyeVal, yREyeVal;
        private int eyeThreshVal;
        // output
        double lEyeAngle = Double.NaN, rEyeAngle = Double.NaN;

        private void StartEyeTracking()
        {
            eyeThread = new BackgroundWorker();
            eyeThread.WorkerReportsProgress = true;
            eyeThread.WorkerSupportsCancellation = true;
            eyeThread.ProgressChanged += new ProgressChangedEventHandler(UpdateUI);
            eyeThread.DoWork += new DoWorkEventHandler(EyeTrackCallback);
            eyeThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(EyeCompleteCallback);
            eyeThread.RunWorkerAsync();
        }

        private void EyeCompleteCallback(object sender, RunWorkerCompletedEventArgs e)
        {
            pictureBox1.Image = null;
            pictureBox1.Refresh();
        }

        private void EyeTrackCallback(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            long[] eyeMoments;
            Stopwatch timerEye = Stopwatch.StartNew();

            while (!worker.CancellationPending)
            {

                /////// wait frame grab from parallel thread
                while (!worker.CancellationPending)
                {
                    if (imgCopyEyesReady.WaitOne(500))
                        break;
                }

                if (Interlocked.Increment(ref imgEyesInUse) == 1 && doTracking)
                {
                    timerEye.Restart();


                    //Track Eyes
                    ///////////// start timed section (~2500 ticks ~ 0.750ms)
                    eyeMoments = MikesUsefulFunctions.StackSafeFloodFillLessPlus2MomentsU8(imgCopyEyes.data, new System.Drawing.Point(imageWidth, imageHeight), new System.Drawing.Point(xLEyeVal, yLEyeVal), (byte)eyeThreshVal, (byte)255);
                    if (eyeMoments != null)
                        lEyeAngle = MikesUsefulFunctions.CalculatePrincipalAxisAngle(eyeMoments);
                    else
                        lEyeAngle = Double.NaN;

                    eyeMoments = MikesUsefulFunctions.StackSafeFloodFillLessPlus2MomentsU8(imgCopyEyes.data, new System.Drawing.Point(imageWidth, imageHeight), new System.Drawing.Point(xREyeVal, yREyeVal), (byte)eyeThreshVal, (byte)255);
                    if (eyeMoments != null)
                        rEyeAngle = MikesUsefulFunctions.CalculatePrincipalAxisAngle(eyeMoments);
                    else
                        rEyeAngle = Double.NaN;
                    //////////// end timed section

                    eyeTime = timerEye.ElapsedTicks;


                    if (doSaveData)
                    {
                        // Write eye data log
                        eyeFileWriter.Write(String.Concat(lEyeAngle.ToString(CultureInfo.GetCultureInfo("en-GB")), "\t"));
                        eyeFileWriter.Write(String.Concat(rEyeAngle.ToString(CultureInfo.GetCultureInfo("en-GB")), "\t"));
                        eyeFileWriter.Write(imgCopyEyes_TimestampSec + "\t");
                        eyeFileWriter.Write(imgCopyEyes_TimestampUs + "\t");
                        eyeFileWriter.Write(imgCopyEyes_Id + "\t");
                        eyeFileWriter.WriteLine();
                    }



                    //Debug.WriteLine(String.Format("eye {0}: {1:0.000}ms",
                    //    imgCopyEyes_Id,
                    //    Math.Round(timerEye.ElapsedTicks / 3350.0, 2)));



                    //Convert image for display
                    ///////////// start timed section (~ 435 ticks ~ 0.13ms) 
                    ellapsedDisplayTime = timerDisplay.ElapsedMilliseconds;
                    if (ellapsedDisplayTime > 17) /// 1/60Hz~17ms
                    {
                        imgCopyEyes.Convert(PixelFormat.PixelFormatBgr, m_processedImage);
                        timerDisplay.Restart();
                        worker.ReportProgress(0);
                    }
                    ///////////// end timed section


                }

                Interlocked.Decrement(ref imgEyesInUse);

            }


        }

        

    }
}
