﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using OpenTK;
using System.Diagnostics;

namespace MicroScopeTracker1
{
    unsafe class MikesUsefulFunctions
    {

        public static long[] StackSafeFloodFillGreaterPlus2MomentsU8(byte* myImage, Point imageSize, Point targetPoint, byte targetColor, byte replacementColor)
        {
            long[] numbersToReturn = new long[6];
            Queue<Point> pointQueue = new Queue<Point>();
            long totalPixelArea = 0;
            long totalPixelX = 0;
            long totalPixelY = 0;
            long totalPixelXX = 0;
            long totalPixelYY = 0;
            long totalPixelXY = 0;
            Point westPoints;
            Point eastPoints;


            if (myImage[getIndex(targetPoint, imageSize)] > targetColor)
            {
                pointQueue.Enqueue(targetPoint);

                while (pointQueue.Count > 0)
                {
                    Point thisPoint = pointQueue.Dequeue();
                    if (myImage[getIndex(thisPoint, imageSize)] > targetColor)
                    {
                        westPoints = thisPoint;
                        eastPoints = thisPoint;
                        while (westPoints.X > 0)
                        {
                            if (myImage[getIndex(westPoints, imageSize)] > targetColor)
                            {
                                myImage[getIndex(westPoints, imageSize)] = replacementColor;
                                totalPixelArea++;
                                totalPixelX += westPoints.X - targetPoint.X;
                                totalPixelY += westPoints.Y - targetPoint.Y;
                                totalPixelXX = (westPoints.X - targetPoint.X) * (westPoints.X - targetPoint.X);
                                totalPixelYY = (westPoints.Y - targetPoint.Y) * (westPoints.Y - targetPoint.Y);
                                totalPixelXY = (westPoints.X - targetPoint.X) * (westPoints.Y - targetPoint.Y);
                                if (westPoints.Y < imageSize.Y - 1)
                                {
                                    if (myImage[getIndex(westPoints + new Size(0, 1), imageSize)] > targetColor) pointQueue.Enqueue(new Point(westPoints.X, westPoints.Y + 1));
                                }
                                if (westPoints.Y > 0)
                                {
                                    if (myImage[getIndex(westPoints + new Size(0, -1), imageSize)] > targetColor) pointQueue.Enqueue(new Point(westPoints.X, westPoints.Y - 1));
                                }

                            }
                            else { westPoints.X = 0; }
                            westPoints.X--;
                        }
                        eastPoints.X++;
                        while (eastPoints.X < imageSize.X)
                        {
                            if (myImage[getIndex(eastPoints, imageSize)] > targetColor)
                            {
                                myImage[getIndex(eastPoints, imageSize)] = replacementColor;
                                totalPixelArea++;
                                totalPixelX += eastPoints.X - targetPoint.X;
                                totalPixelY += eastPoints.Y - targetPoint.Y;
                                totalPixelXX = (eastPoints.X - targetPoint.X) * (eastPoints.X - targetPoint.X);
                                totalPixelYY = (eastPoints.Y - targetPoint.Y) * (eastPoints.Y - targetPoint.Y);
                                totalPixelXY = (eastPoints.X - targetPoint.X) * (eastPoints.Y - targetPoint.Y);
                                if (eastPoints.Y < imageSize.Y - 1)
                                {
                                    if (myImage[getIndex(eastPoints + new Size(0, 1), imageSize)] > targetColor) pointQueue.Enqueue(new Point(eastPoints.X, eastPoints.Y + 1));
                                }
                                if (eastPoints.Y > 0)
                                {
                                    if (myImage[getIndex(eastPoints + new Size(0, -1), imageSize)] > targetColor) pointQueue.Enqueue(new Point(eastPoints.X, eastPoints.Y - 1));
                                }

                            }
                            else { eastPoints.X = imageSize.X; }
                            eastPoints.X++;
                        }



                    }

                }

            }
            numbersToReturn[0] = totalPixelArea;
            numbersToReturn[1] = totalPixelX;
            numbersToReturn[2] = totalPixelY;
            numbersToReturn[3] = totalPixelXX;
            numbersToReturn[4] = totalPixelYY;
            numbersToReturn[5] = totalPixelXY;
            return numbersToReturn;
        }

        /// <summary>
        /// Can process 4800px/ms in Intel i7-4770 3.4 GHz. 
        /// Function paints detected ROI in image giver in argument.
        /// </summary>
        /// <param name="myImage"></param>
        /// <param name="imageSize"></param>
        /// <param name="targetPoint"></param>
        /// <param name="targetColor"></param>
        /// <param name="replacementColor"></param>
        /// <returns></returns>
        public static long[] StackSafeFloodFillLessPlus2MomentsU8(byte* myImage, Point imageSize, Point targetPoint, byte targetColor, byte replacementColor)
        {
            long[] numbersToReturn = new long[6];
            Queue<Point> pointQueue = new Queue<Point>();
            long totalPixelArea = 0;
            long totalPixelX = 0;
            long totalPixelY = 0;
            long totalPixelXX = 0;
            long totalPixelYY = 0;
            long totalPixelXY = 0;
            Point westPoints;
            Point eastPoints;
            Stopwatch timerDebug = Stopwatch.StartNew();
            int iterCounter = 0;
            int timeoutTicks = 150000; // 1ms ~ 3500 ticks


            if (myImage[getIndex(targetPoint, imageSize)] < targetColor)
            {
                pointQueue.Enqueue(targetPoint);

                while (pointQueue.Count > 0)
                {
                    
                    Point thisPoint = pointQueue.Dequeue();
                    if (myImage[getIndex(thisPoint, imageSize)] < targetColor)
                    {
                        westPoints = thisPoint;
                        eastPoints = thisPoint;
                        while (westPoints.X > 0)
                        {
                            iterCounter++;
                            if (myImage[getIndex(westPoints, imageSize)] < targetColor)
                            {
                                myImage[getIndex(westPoints, imageSize)] = replacementColor; // edit original image

                                totalPixelArea++;
                                totalPixelX += westPoints.X - targetPoint.X;
                                totalPixelY += westPoints.Y - targetPoint.Y;
                                totalPixelXX = (westPoints.X - targetPoint.X) * (westPoints.X - targetPoint.X);
                                totalPixelYY = (westPoints.Y - targetPoint.Y) * (westPoints.Y - targetPoint.Y);
                                totalPixelXY = (westPoints.X - targetPoint.X) * (westPoints.Y - targetPoint.Y);
                                if (westPoints.Y < imageSize.Y - 1)
                                {
                                    if (myImage[getIndex(westPoints + new Size(0, 1), imageSize)] < targetColor) pointQueue.Enqueue(new Point(westPoints.X, westPoints.Y + 1));
                                }
                                if (westPoints.Y > 0)
                                {
                                    if (myImage[getIndex(westPoints + new Size(0, -1), imageSize)] < targetColor) pointQueue.Enqueue(new Point(westPoints.X, westPoints.Y - 1));
                                }

                            }
                            else { westPoints.X = 0; }
                            westPoints.X--;

                            // Jose: safe code to limit computation time
                            //if(iterCounter > 2000)
                            if (timerDebug.ElapsedTicks > timeoutTicks)
                            {
                                //Debug.WriteLine(timerDebug.ElapsedTicks);
                                return null;
                            }
                        }
                        eastPoints.X++;
                        while (eastPoints.X < imageSize.X)
                        {
                            iterCounter++;

                            if (myImage[getIndex(eastPoints, imageSize)] < targetColor)
                            {
                                myImage[getIndex(eastPoints, imageSize)] = replacementColor; // edit original image

                                totalPixelArea++;
                                totalPixelX += eastPoints.X - targetPoint.X;
                                totalPixelY += eastPoints.Y - targetPoint.Y;
                                totalPixelXX = (eastPoints.X - targetPoint.X) * (eastPoints.X - targetPoint.X);
                                totalPixelYY = (eastPoints.Y - targetPoint.Y) * (eastPoints.Y - targetPoint.Y);
                                totalPixelXY = (eastPoints.X - targetPoint.X) * (eastPoints.Y - targetPoint.Y);
                                if (eastPoints.Y < imageSize.Y - 1)
                                {
                                    if (myImage[getIndex(eastPoints + new Size(0, 1), imageSize)] < targetColor) pointQueue.Enqueue(new Point(eastPoints.X, eastPoints.Y + 1));
                                }
                                if (eastPoints.Y > 0)
                                {
                                    if (myImage[getIndex(eastPoints + new Size(0, -1), imageSize)] < targetColor) pointQueue.Enqueue(new Point(eastPoints.X, eastPoints.Y - 1));
                                }

                            }
                            else { eastPoints.X = imageSize.X; }
                            eastPoints.X++;

                            // Jose: safe code to limit computation time
                            if (timerDebug.ElapsedTicks > timeoutTicks)
                            {
                                return null;
                            }
                        }



                    }

                }
                //Debug.WriteLine("px:" + iterCounter + " ticks:" + timerDebug.ElapsedTicks);

            } //end threshold condition
            numbersToReturn[0] = totalPixelArea;
            numbersToReturn[1] = totalPixelX;
            numbersToReturn[2] = totalPixelY;
            numbersToReturn[3] = totalPixelXX;
            numbersToReturn[4] = totalPixelYY;
            numbersToReturn[5] = totalPixelXY;

            
            return numbersToReturn;
        }

        private static int getIndex(int X, int Y, Point imageSize)
        {
            return X + Y * imageSize.X;
        }
        private static int getIndex(Point myPoint, Point imageSize)
        {
            return myPoint.X + myPoint.Y * imageSize.X;
        }

        public static double CalculatePrincipalAxisAngle(long[] rawBlobMoments)
        {
            double m00 = (double)rawBlobMoments[0];
            double m10 = (double)rawBlobMoments[1];
            double m01 = (double)rawBlobMoments[2];
            double m11 = (double)rawBlobMoments[5];
            double m20 = (double)rawBlobMoments[3];
            double m02 = (double)rawBlobMoments[4];

            double mu11 = m11 - (m10 * m01 / m00);
            double mu20 = m20 - (m10 * m10 / m00);
            double mu02 = m02 - (m01 * m01 / m00);

            mu11 /= m00;
            mu20 /= m00;
            mu02 /= m00;
            double thisAngle = Math.Atan2((-2.0 * mu11), (mu20 - mu02)) / 2.0d;
            //if ((mu20 - mu02) < 0)
            //{
            //    if (mu11 > 0)
            //    {
            //        thisAngle += Math.PI / 2.0d;
            //    }
            //    else
            //    {

            //        thisAngle -= Math.PI / 2.0d;

            //    }
            //}

            return thisAngle;

        }
        public static Vector2 rotateVectorRadians(Vector2 startVector, float rotAngle)
        {
            Vector2 newVector = new Vector2(1.0f, 1.0f);
            newVector.X = (float)(startVector.X * Math.Cos(rotAngle) - startVector.Y * Math.Sin(rotAngle));
            newVector.Y = (float)(startVector.X * Math.Sin(rotAngle) + startVector.Y * Math.Cos(rotAngle));
            return newVector;
        }

        public static float bilinearInterpolatePointer(byte* targetImage, Vector2 targetPoint, Point imageSize)
        {
            double floorX = Math.Floor(targetPoint.X);
            double floorY = Math.Floor(targetPoint.Y);
            double x = targetPoint.X - floorX;
            double y = targetPoint.Y - floorY;
            float f00 = (float)targetImage[((int)floorX) + ((int)floorY) * imageSize.X];
            float f01 = (float)targetImage[((int)floorX) + ((int)floorY + 1) * imageSize.X];
            float f10 = (float)targetImage[((int)floorX + 1) + ((int)floorY) * imageSize.X];
            float f11 = (float)targetImage[((int)floorX + 1) + ((int)floorY + 1) * imageSize.X];
            return (float)(f00 * (1 - x) * (1 - y) + f10 * (x) * (1 - y) + f01 * (1 - x) * (y) + f11 * x * y);
        }

        public static float[] findCenterofMassOfArcPointer(byte* targetImage, Point imageSize, Vector2 startPoint, Vector2 startVector, float angle, byte threshold)
        {
            float weightedMass = 0, totalMass = 0;
            float thisWeight;
            float[] returnValues = new float[2];

            Vector2 thisPoint = new Vector2(1.0f, 1.0f);

            for (float thisAngle = -angle; thisAngle <= angle; thisAngle += 2 * angle / (float)Math.Ceiling(startVector.Length))
            {
                thisPoint = startPoint + rotateVectorRadians(startVector, thisAngle);
                if (((Math.Floor(thisPoint.X) + 1) < imageSize.X) & ((Math.Floor(thisPoint.Y) + 1) < imageSize.Y) & (Math.Floor(thisPoint.X) >= 0) & (Math.Floor(thisPoint.Y) >= 0))
                {
                    if ((thisWeight = bilinearInterpolatePointer(targetImage, thisPoint, imageSize)) > threshold)
                    {
                        weightedMass += thisAngle * thisWeight;
                        totalMass += thisWeight;
                    }
                }

            }
            returnValues[0] = totalMass;
            if (totalMass > 0) returnValues[1] = weightedMass / totalMass;
            else returnValues[1] = 0;
            return returnValues;

        }


        public static float[] returnArcPointer(byte* targetImage, Point imageSize, Vector2 startPoint, Vector2 startVector, float angle,int numSteps)
        {
            
            float[] returnValues = new float[numSteps];

            Vector2 thisPoint = new Vector2(1.0f, 1.0f);
            float thisAngle;
            
            for (int ii = 0; ii<numSteps; ii++)
                {
                    thisAngle = -angle + 2 * angle * ii / ((float)numSteps - 1.0f);
                thisPoint = startPoint + rotateVectorRadians(startVector, thisAngle);
                if (((Math.Floor(thisPoint.X) + 1) < imageSize.X) & ((Math.Floor(thisPoint.Y) + 1) < imageSize.Y) & (Math.Floor(thisPoint.X) >= 0) & (Math.Floor(thisPoint.Y) >= 0))
                {
                    returnValues[ii] = bilinearInterpolatePointer(targetImage, thisPoint, imageSize);
                   // targetImage[getIndex((int) thisPoint.X,(int) thisPoint.Y,imageSize)]=255;
                }
                

            }
            return returnValues;

        }

        public static bool CheckBounds(Point imageSize, Point myPoint)
        {
            if ((myPoint.X >= 0) & (myPoint.Y >= 0) & (myPoint.X < imageSize.X) & (myPoint.Y < imageSize.Y))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
  
    }
}
