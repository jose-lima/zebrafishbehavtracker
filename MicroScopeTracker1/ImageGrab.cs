﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.ComponentModel;
using FlyCapture2Managed;
using System.Runtime.InteropServices;
using System.Collections;


namespace MicroScopeTracker1
{
    public unsafe partial class Form1 : Form
    {
     
        byte[] imgBeingProcessedBytes;

        // Image tail copy
        //ManagedImage imgCopyTail = new ManagedImage();
        AutoResetEvent imgCopyTailReady = new AutoResetEvent(false);
        private int imgTailInUse = 0;
        private uint imgCopyTail_Id_start, imgCopyTail_TimestampUs_start;
        private long imgCopyTail_TimestampSec_start;
        //long imgCopyTail_Id = -1;
        //long imgCopyTail_TimestampSec = -1;
        //long imgCopyTail_TimestampUs = -1;
        // Image eyes copy
        ManagedImage imgCopyEyes = new ManagedImage();
        AutoResetEvent imgCopyEyesReady = new AutoResetEvent(false);
        private int imgEyesInUse = 0;
        private uint imgCopyEyes_Id_start, imgCopyEyes_TimestampUs_start;
        private long imgCopyEyes_TimestampSec_start;
        long imgCopyEyes_Id = -1;
        long imgCopyEyes_TimestampSec = -1;
        long imgCopyEyes_TimestampUs = -1;

        Queue<MyImage> tailImgQueue;

        private bool StartAcquisition()
        {
            try
            {
                m_camera.StartCapture();
                return true;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
                return false;
            }
        }

        private void StartGrabLoop()
        {
            m_grabThread = new BackgroundWorker();
            m_grabThread.ProgressChanged += new ProgressChangedEventHandler(UpdateUI);
            m_grabThread.DoWork += new DoWorkEventHandler(GrabLoop);
            m_grabThread.WorkerReportsProgress = true;
            m_grabThread.WorkerSupportsCancellation = true;
            m_grabThread.RunWorkerAsync();

        }
        
        private void GrabLoop(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            Stopwatch debugTimer = Stopwatch.StartNew();
            m_rawImage = new ManagedImage();
            m_processedImage = new ManagedImage();
            long grabLoopNr = 0;

            tailImgQueue = new Queue<MyImage>();

            while (!worker.CancellationPending)
            {

                //Debug.WriteLine("grab time:" + debugTimer.ElapsedMilliseconds + " ticks:" + debugTimer.ElapsedTicks);
                //debugTimer.Restart();



                ///////////// start timed section (~ 3600 some times up to 8000ticks ~ 1.07 to 2.4ms)  
                try
                {
                    lock (m_rawImage)
                    {
                        // get image from camera
                        m_camera.RetrieveBuffer(m_rawImage); //blocking for ~1/fps
                        grabLoopNr++;

                        // get image info
                        if (grabLoopNr == 1)
                        {
                            imageWidth = (int)m_rawImage.cols;
                            imageHeight = (int)m_rawImage.rows;
                        }


                        // Copy image for tail queue
                        //if (Interlocked.Increment(ref imgTailInUse) == 1)
                        //{
                        ManagedImage imgCopyTail = new ManagedImage(m_rawImage);
                        if (grabLoopNr==1)
                        {
                            imgCopyTail_Id_start = m_rawImage.imageMetadata.embeddedFrameCounter;
                            imgCopyTail_TimestampSec_start = m_rawImage.timeStamp.seconds;
                            imgCopyTail_TimestampUs_start = m_rawImage.timeStamp.microSeconds;
                        }
                        long imgCopyTail_Id = m_rawImage.imageMetadata.embeddedFrameCounter- imgCopyTail_Id_start;
                        long imgCopyTail_TimestampSec = m_rawImage.timeStamp.seconds - imgCopyTail_TimestampSec_start;
                        long imgCopyTail_TimestampUs = m_rawImage.timeStamp.microSeconds;

                        lock(tailImgQueue)
                        {
                            tailImgQueue.Enqueue(new MyImage(imgCopyTail, imgCopyTail_Id, imgCopyTail_TimestampSec, imgCopyTail_TimestampUs));
                        }
                        //Debug.WriteLine(imgCopyTail_Id);
                        //}
                        //Interlocked.Decrement(ref imgTailInUse);
                        imgCopyTailReady.Set();




                        // Copy image for eyes processing only if not processing it
                        if (Interlocked.Increment(ref imgEyesInUse) == 1)
                        {
                            imgCopyEyes = new ManagedImage(m_rawImage);
                            if (grabLoopNr == 1)
                            {
                                imgCopyEyes_Id_start = m_rawImage.imageMetadata.embeddedFrameCounter;
                                imgCopyEyes_TimestampSec_start = m_rawImage.timeStamp.seconds;
                                imgCopyEyes_TimestampUs_start = m_rawImage.timeStamp.microSeconds;
                            }
                            imgCopyEyes_Id = m_rawImage.imageMetadata.embeddedFrameCounter - imgCopyEyes_Id_start;
                            imgCopyEyes_TimestampSec = m_rawImage.timeStamp.seconds - imgCopyEyes_TimestampSec_start;
                            imgCopyEyes_TimestampUs = m_rawImage.timeStamp.microSeconds;
                          
                        }
                        Interlocked.Decrement(ref imgEyesInUse);
                        imgCopyEyesReady.Set();

                    }

                }
                catch (FC2Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    continue;
                }
                ///////////// end timed section


            }


            StopCamera();

        }

        /// <summary>
        /// 140 ticks some times 1400 ticks.
        /// </summary>
        private void CopyImageBytes()
        {
            int imgLen = (int)m_rawImage.rows * (int)m_rawImage.stride;
            imgBeingProcessedBytes = new byte[imgLen];
            Marshal.Copy((IntPtr)m_rawImage.data, imgBeingProcessedBytes, 0, imgLen);
        }

        private void StopCamera()
        {
            if (m_camera != null)
            {
                try
                {
                    m_camera.StopCapture();
                    Debug.WriteLine("Acquisition stoped.");
                }
                catch (Exception exc)
                {
                    Debug.WriteLine(exc.Message);
                }
            }
        }

        //private void GrabImage()
        //{
        //    m_camera.RetrieveBuffer(m_rawImage);

        //    // get frame info
        //    imgCopyTail_Id = m_rawImage.imageMetadata.embeddedFrameCounter;
        //    imgCopyTail_TimestampSec = m_rawImage.timeStamp.seconds;
        //    imgCopyTail_TimestampUs = m_rawImage.timeStamp.microSeconds;
        //}

        public class MyImage
        {
            public ManagedImage imgCopyTail = new ManagedImage();
            public long imgCopyTail_Id = -1;
            public long imgCopyTail_TimestampSec = -1;
            public long imgCopyTail_TimestampUs = -1;

            public MyImage(ManagedImage imgCopyTail, long imgCopyTail_Id, long imgCopyTail_TimestampSec, long imgCopyTail_TimestampUs)
            {
                this.imgCopyTail = imgCopyTail;
                this.imgCopyTail_Id = imgCopyTail_Id;
                this.imgCopyTail_TimestampSec = imgCopyTail_TimestampSec;
                this.imgCopyTail_TimestampUs = imgCopyTail_TimestampUs;
            }
        }


    }
}
