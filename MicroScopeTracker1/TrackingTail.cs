﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.ComponentModel;
using FlyCapture2Managed;
using System.Globalization;
using OpenTK;

namespace MicroScopeTracker1
{
    public unsafe partial class Form1 : Form
    {
        // Processing loop logic
        BackgroundWorker tailThread, eyeThread;
        Stopwatch timerDisplayImg = Stopwatch.StartNew();
        long ellapsedDisplayTime = 0;
        long tailTime = 0;
        long eyeTime = 0;

        // tracking parameters
        private int xScanTopVal, yScanTopVal, xScanBotVal, yScanBotVal;
        private int xStartVal, yStartVal, xInitVal, yInitVal;

        string trackerFilePath = "tracker.xml";

        public class Tracker
        {
            public string savePath;
            public bool doTracking;
            public bool doSaveData;
        }

        private void StartProcThreads()
        {
            StartTailTracking();
            StartEyeTracking();
        }

        private void StartTailTracking()
        {
            tailThread = new BackgroundWorker();
            tailThread.WorkerReportsProgress = true;
            tailThread.WorkerSupportsCancellation = true;
            //tailThread.ProgressChanged += new ProgressChangedEventHandler(UpdateUI);
            tailThread.DoWork += new DoWorkEventHandler(TailTrackCallback);
            //tailThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ProcCompleteCallback);
            tailThread.RunWorkerAsync();
        }

        private void StopProcLoop()
        {
            if (tailThread != null && tailThread.IsBusy)
            {
                tailThread.CancelAsync();
            }

            if (eyeThread != null && eyeThread.IsBusy)
            {
                eyeThread.CancelAsync();
            }
        }

        private void TailTrackCallback(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            Stopwatch debugTimer = Stopwatch.StartNew();
            int[] laserLine;
            byte[] laserLineByte;
            int[] tailArc;

            int tailLoopNumber = 0;
            Stopwatch myStopwatch = new Stopwatch();
            myStopwatch.Start();
            long time1 = 0, time2 = 0, time3 = 0, time4 = 0, time5 = 0;
            //tail search parameters
            int searchLength = 200; //previously 150
            int numSteps = (int)Math.Ceiling(Math.PI * (double)searchLength);
            int searchLength2 = 300; //previously 150
            int numSteps2 = (int)Math.Ceiling(Math.PI * (double)searchLength2);
            long lastProcFrame = -1;
            MyImage presImg;

            while (!worker.CancellationPending)
            {

                /////// wait frame grab from parallel thread
                while (!worker.CancellationPending)
                {
                    if (imgCopyTailReady.WaitOne(500))
                        break;
                }
                //if (imgCopyTail_Id == lastProcFrame)
                //    continue;
                //lastProcFrame = imgCopyTail_Id;
                if (tailImgQueue.Count == 0)
                    continue;
                lock (tailImgQueue)
                {
                    presImg = tailImgQueue.Dequeue();
                }


                debugTimer.Restart();
                //if (Interlocked.Increment(ref imgTailInUse) == 1)                        
                //{
                //Debug.WriteLine("procFrame: "+inProcFrameId);
                if (doTracking)
                {
                    byte[] tailArch1Values = new byte[numSteps];
                    // Clau added tail search parameters for second arc                                               
                    byte[] tailArch2Values2 = new byte[numSteps2];
                    // end clau added
                    laserLine = new int[yScanBotVal - yScanTopVal + 1];
                    laserLineByte = new byte[yScanBotVal - yScanTopVal + 1];





                    // Track laser
                    for (int ii = yScanTopVal; ii <= yScanBotVal; ii++)
                    {
                        laserLine[ii - yScanTopVal] = 0;
                        for (int jj = xScanTopVal; jj <= xScanBotVal; jj++)
                        {
                            laserLine[ii - yScanTopVal] += presImg.imgCopyTail.data[jj + ii * imageWidth];
                        }
                        for (int jj = 0; jj < 5; jj++)
                        {
                            laserLineByte[ii - yScanTopVal] = (byte)(laserLine[ii - yScanTopVal] / (xScanBotVal - xScanTopVal + 1));
                            presImg.imgCopyTail.data[ii - yScanTopVal + (imageHeight - 1 - jj) * imageWidth] = laserLineByte[ii - yScanTopVal];
                        }
                    }
                    ///////////// end timed section
                    time1 = debugTimer.ElapsedTicks;






                    //Tail tracking
                    ///////////// start timed section (~3800 ticks ~ 1.14ms)
                    Vector2 startVector = new Vector2(xInitVal, yInitVal);
                    Vector2 normSearchVector;
                    Vector2 searchVector;
                    //arc length was 150 now 200 pixels
                    float stretchTop = 155.0f;
                    float stretchBot = 30.0f;
                    Vector2.Normalize(ref startVector, out normSearchVector);
                    float[] arcValues = new float[numSteps];
                    float[] nextValues = new float[numSteps];
                    int segwidth = 2;
                    for (int ii = -segwidth; ii <= segwidth; ii++)
                    {
                        Vector2.Multiply(ref normSearchVector, (float)(searchLength + ii), out searchVector);
                        nextValues = MikesUsefulFunctions.returnArcPointer(presImg.imgCopyTail.data, new System.Drawing.Point(imageWidth, imageHeight), new Vector2(xStartVal, yStartVal), searchVector, fpi / 2.0f, numSteps);
                        for (int jj = 0; jj < numSteps; jj++)
                        {
                            arcValues[jj] += nextValues[jj];
                        }
                    }
                    for (int jj = 0; jj < numSteps; jj++)
                    {
                        tailArch1Values[jj] = (byte)(arcValues[jj] / (float)((segwidth * 2) + 1));
                        for (int kk = 15; kk < 30; kk++)
                        {
                            presImg.imgCopyTail.data[jj + (imageHeight - 1 - kk) * imageWidth] = tailArch1Values[jj];
                        }
                    }
                    ///////////// end timed section
                    time2 = debugTimer.ElapsedTicks;








                    // Clau added second arc -- 2015-07-18
                    ///////////// start timed section (~6200 ticks ~ 1.85ms)
                    Vector2 startVector2 = new Vector2(xInitVal, yInitVal);
                    Vector2 normSearchVector2;
                    Vector2 searchVector2;

                    Vector2.Normalize(ref startVector2, out normSearchVector2);
                    float[] arcValues2 = new float[numSteps2];
                    float[] nextValues2 = new float[numSteps2];
                    for (int ii = -segwidth; ii <= segwidth; ii++)
                    {
                        Vector2.Multiply(ref normSearchVector2, (float)(searchLength2 + ii), out searchVector2);
                        nextValues2 = MikesUsefulFunctions.returnArcPointer(presImg.imgCopyTail.data, new System.Drawing.Point(imageWidth, imageHeight), new Vector2(xStartVal, yStartVal), searchVector2, fpi / 2.0f, numSteps2);
                        for (int jj = 0; jj < numSteps2; jj++)
                        {
                            arcValues2[jj] += nextValues2[jj];
                        }
                    }
                    for (int jj = 0; jj < numSteps2; jj++)
                    {
                        tailArch2Values2[jj] = (byte)(arcValues2[jj] / (float)((segwidth * 2) + 1));
                        for (int kk = 30; kk < 60; kk++)
                        {
                            presImg.imgCopyTail.data[jj + (imageHeight - 1 - kk) * imageWidth] = tailArch2Values2[jj];
                        }
                    }
                    // end Clau added
                    ///////////// end timed section
                    time3 = debugTimer.ElapsedTicks;







                    ///////////// start timed section (~30 to 130 ticks ~ max. 0.04ms)
                    //for (float thisAngle = -angle; thisAngle <= angle; thisAngle += 2 * angle / (float)Math.Ceiling(startVector.Length))
                    if (doSaveData)
                    {

                        if (firstSave)
                        {
                            tailArchOneTwoLaserWriter.Write((int)(numSteps + numSteps2 + laserLine.Length));
                            firstSave = false;
                        }

                        tailArchOneTwoLaserWriter.Write(tailArch1Values);
                        tailArchOneTwoLaserWriter.Write(tailArch2Values2);
                        tailArchOneTwoLaserWriter.Write(laserLineByte);

                        tailTimestampsWriter.Write(presImg.imgCopyTail_TimestampSec + "\t");
                        tailTimestampsWriter.Write(presImg.imgCopyTail_TimestampUs + "\t");
                        tailTimestampsWriter.Write(presImg.imgCopyTail_Id + "\t");
                        tailTimestampsWriter.WriteLine();
                    }

                }
                ///////////// end timed section
                time4 = debugTimer.ElapsedTicks;



                tailLoopNumber++;





                tailTime = debugTimer.ElapsedTicks;
                //  Debug.WriteLine("proc time:" + debugTimer.ElapsedMilliseconds + " ticks:" + debugTimer.ElapsedTicks);

                //Debug.WriteLine(String.Format("tailUs: laser:{0:0.00} tail1:{1:0.00} tail2:{2:0.00} save:{3:0.00} tot:{4:0.00} f:{5} queueSize:{6}",
                //    time1 * 1000.0 / 3350,
                //    (time2 - time1) * 1000.0 / 3350,
                //    (time3 - time2) * 1000.0 / 3350,
                //    (time4 - time3) * 1000.0 / 3350,
                //    tailTime * 1000.0 / 3350,
                //    presImg.imgCopyTail_Id,
                //    tailImgQueue.Count
                //    ));


                //} //end of lock
                //Interlocked.Decrement(ref imgTailInUse);


            }

        }


    }



}
