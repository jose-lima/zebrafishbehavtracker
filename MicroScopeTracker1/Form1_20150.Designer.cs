﻿namespace MicroScopeTracker1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startCameraButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.eyeThresholdLabel = new System.Windows.Forms.Label();
            this.eyeThreshold = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.yREye = new System.Windows.Forms.NumericUpDown();
            this.xREye = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.yLEye = new System.Windows.Forms.NumericUpDown();
            this.xLEye = new System.Windows.Forms.NumericUpDown();
            this.clickCaliButton = new System.Windows.Forms.Button();
            this.yInitLabel = new System.Windows.Forms.Label();
            this.xInitLabel = new System.Windows.Forms.Label();
            this.yInit = new System.Windows.Forms.NumericUpDown();
            this.xInit = new System.Windows.Forms.NumericUpDown();
            this.yStartLabel = new System.Windows.Forms.Label();
            this.xStartLabel = new System.Windows.Forms.Label();
            this.yStart = new System.Windows.Forms.NumericUpDown();
            this.xStart = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.yScanBot = new System.Windows.Forms.NumericUpDown();
            this.xScanBot = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.yScanTop = new System.Windows.Forms.NumericUpDown();
            this.xScanTop = new System.Windows.Forms.NumericUpDown();
            this.saveDefaultParams = new System.Windows.Forms.Button();
            this.startTrackingButton = new System.Windows.Forms.Button();
            this.startSaveButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eyeThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yREye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xREye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yLEye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xLEye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yScanBot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xScanBot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yScanTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xScanTop)).BeginInit();
            this.SuspendLayout();
            // 
            // startCameraButton
            // 
            this.startCameraButton.Location = new System.Drawing.Point(462, 12);
            this.startCameraButton.Name = "startCameraButton";
            this.startCameraButton.Size = new System.Drawing.Size(92, 34);
            this.startCameraButton.TabIndex = 0;
            this.startCameraButton.Text = "Start Camera";
            this.startCameraButton.UseVisualStyleBackColor = true;
            this.startCameraButton.Click += new System.EventHandler(this.startCameraButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(433, 263);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            // 
            // eyeThresholdLabel
            // 
            this.eyeThresholdLabel.AutoSize = true;
            this.eyeThresholdLabel.Location = new System.Drawing.Point(10, 308);
            this.eyeThresholdLabel.Name = "eyeThresholdLabel";
            this.eyeThresholdLabel.Size = new System.Drawing.Size(75, 13);
            this.eyeThresholdLabel.TabIndex = 54;
            this.eyeThresholdLabel.Text = "Eye Threshold";
            // 
            // eyeThreshold
            // 
            this.eyeThreshold.Location = new System.Drawing.Point(91, 306);
            this.eyeThreshold.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.eyeThreshold.Name = "eyeThreshold";
            this.eyeThreshold.Size = new System.Drawing.Size(94, 20);
            this.eyeThreshold.TabIndex = 53;
            this.eyeThreshold.Value = new decimal(new int[] {
            107,
            0,
            0,
            0});
            this.eyeThreshold.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(199, 360);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 52;
            this.label9.Text = "R Eye Y";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label10.Location = new System.Drawing.Point(199, 334);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 51;
            this.label10.Text = "R Eye X";
            // 
            // yREye
            // 
            this.yREye.Location = new System.Drawing.Point(268, 353);
            this.yREye.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yREye.Name = "yREye";
            this.yREye.Size = new System.Drawing.Size(94, 20);
            this.yREye.TabIndex = 50;
            this.yREye.Value = new decimal(new int[] {
            199,
            0,
            0,
            0});
            this.yREye.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xREye
            // 
            this.xREye.Location = new System.Drawing.Point(268, 327);
            this.xREye.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xREye.Name = "xREye";
            this.xREye.Size = new System.Drawing.Size(94, 20);
            this.xREye.TabIndex = 49;
            this.xREye.Value = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.xREye.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 367);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 48;
            this.label7.Text = "L Eye Y";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label8.Location = new System.Drawing.Point(21, 341);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 47;
            this.label8.Text = "L Eye X";
            // 
            // yLEye
            // 
            this.yLEye.Location = new System.Drawing.Point(90, 360);
            this.yLEye.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yLEye.Name = "yLEye";
            this.yLEye.Size = new System.Drawing.Size(94, 20);
            this.yLEye.TabIndex = 46;
            this.yLEye.Value = new decimal(new int[] {
            199,
            0,
            0,
            0});
            this.yLEye.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xLEye
            // 
            this.xLEye.Location = new System.Drawing.Point(90, 334);
            this.xLEye.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xLEye.Name = "xLEye";
            this.xLEye.Size = new System.Drawing.Size(94, 20);
            this.xLEye.TabIndex = 45;
            this.xLEye.Value = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.xLEye.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // clickCaliButton
            // 
            this.clickCaliButton.Location = new System.Drawing.Point(400, 327);
            this.clickCaliButton.Name = "clickCaliButton";
            this.clickCaliButton.Size = new System.Drawing.Size(154, 55);
            this.clickCaliButton.TabIndex = 55;
            this.clickCaliButton.Text = "Click To Calibrate SB Tail Top Bot LE RE";
            this.clickCaliButton.UseVisualStyleBackColor = true;
            this.clickCaliButton.Click += new System.EventHandler(this.clickCalliButton_Click);
            // 
            // yInitLabel
            // 
            this.yInitLabel.AutoSize = true;
            this.yInitLabel.Location = new System.Drawing.Point(456, 137);
            this.yInitLabel.Name = "yInitLabel";
            this.yInitLabel.Size = new System.Drawing.Size(51, 13);
            this.yInitLabel.TabIndex = 63;
            this.yInitLabel.Text = "Y Search";
            // 
            // xInitLabel
            // 
            this.xInitLabel.AutoSize = true;
            this.xInitLabel.Location = new System.Drawing.Point(454, 111);
            this.xInitLabel.Name = "xInitLabel";
            this.xInitLabel.Size = new System.Drawing.Size(51, 13);
            this.xInitLabel.TabIndex = 62;
            this.xInitLabel.Text = "X Search";
            // 
            // yInit
            // 
            this.yInit.Location = new System.Drawing.Point(528, 130);
            this.yInit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yInit.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.yInit.Name = "yInit";
            this.yInit.Size = new System.Drawing.Size(94, 20);
            this.yInit.TabIndex = 61;
            this.yInit.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.yInit.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xInit
            // 
            this.xInit.Location = new System.Drawing.Point(528, 104);
            this.xInit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xInit.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.xInit.Name = "xInit";
            this.xInit.Size = new System.Drawing.Size(94, 20);
            this.xInit.TabIndex = 60;
            this.xInit.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // yStartLabel
            // 
            this.yStartLabel.AutoSize = true;
            this.yStartLabel.Location = new System.Drawing.Point(459, 85);
            this.yStartLabel.Name = "yStartLabel";
            this.yStartLabel.Size = new System.Drawing.Size(39, 13);
            this.yStartLabel.TabIndex = 59;
            this.yStartLabel.Text = "Y Start";
            // 
            // xStartLabel
            // 
            this.xStartLabel.AutoSize = true;
            this.xStartLabel.Location = new System.Drawing.Point(459, 59);
            this.xStartLabel.Name = "xStartLabel";
            this.xStartLabel.Size = new System.Drawing.Size(39, 13);
            this.xStartLabel.TabIndex = 58;
            this.xStartLabel.Text = "X Start";
            // 
            // yStart
            // 
            this.yStart.Location = new System.Drawing.Point(528, 78);
            this.yStart.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yStart.Name = "yStart";
            this.yStart.Size = new System.Drawing.Size(94, 20);
            this.yStart.TabIndex = 57;
            this.yStart.Value = new decimal(new int[] {
            199,
            0,
            0,
            0});
            this.yStart.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xStart
            // 
            this.xStart.Location = new System.Drawing.Point(528, 52);
            this.xStart.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xStart.Name = "xStart";
            this.xStart.Size = new System.Drawing.Size(94, 20);
            this.xStart.TabIndex = 56;
            this.xStart.Value = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.xStart.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(456, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 71;
            this.label1.Text = "Scan BR y";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(454, 224);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 70;
            this.label2.Text = "Scan BR x";
            // 
            // yScanBot
            // 
            this.yScanBot.Location = new System.Drawing.Point(528, 243);
            this.yScanBot.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yScanBot.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.yScanBot.Name = "yScanBot";
            this.yScanBot.Size = new System.Drawing.Size(94, 20);
            this.yScanBot.TabIndex = 69;
            this.yScanBot.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.yScanBot.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xScanBot
            // 
            this.xScanBot.Location = new System.Drawing.Point(528, 217);
            this.xScanBot.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xScanBot.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.xScanBot.Name = "xScanBot";
            this.xScanBot.Size = new System.Drawing.Size(94, 20);
            this.xScanBot.TabIndex = 68;
            this.xScanBot.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(459, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 67;
            this.label3.Text = "Scan TL y";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(459, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 66;
            this.label4.Text = "Scan TL x";
            // 
            // yScanTop
            // 
            this.yScanTop.Location = new System.Drawing.Point(528, 191);
            this.yScanTop.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yScanTop.Name = "yScanTop";
            this.yScanTop.Size = new System.Drawing.Size(94, 20);
            this.yScanTop.TabIndex = 65;
            this.yScanTop.Value = new decimal(new int[] {
            199,
            0,
            0,
            0});
            this.yScanTop.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // xScanTop
            // 
            this.xScanTop.Location = new System.Drawing.Point(528, 165);
            this.xScanTop.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.xScanTop.Name = "xScanTop";
            this.xScanTop.Size = new System.Drawing.Size(94, 20);
            this.xScanTop.TabIndex = 64;
            this.xScanTop.Value = new decimal(new int[] {
            330,
            0,
            0,
            0});
            this.xScanTop.ValueChanged += new System.EventHandler(this.xStart_ValueChanged);
            // 
            // saveDefaultParams
            // 
            this.saveDefaultParams.Location = new System.Drawing.Point(560, 327);
            this.saveDefaultParams.Name = "saveDefaultParams";
            this.saveDefaultParams.Size = new System.Drawing.Size(62, 55);
            this.saveDefaultParams.TabIndex = 72;
            this.saveDefaultParams.Text = "Set As Default";
            this.saveDefaultParams.UseVisualStyleBackColor = true;
            this.saveDefaultParams.Click += new System.EventHandler(this.saveDefaultParams_Click);
            // 
            // startTrackingButton
            // 
            this.startTrackingButton.Location = new System.Drawing.Point(560, 12);
            this.startTrackingButton.Name = "startTrackingButton";
            this.startTrackingButton.Size = new System.Drawing.Size(87, 34);
            this.startTrackingButton.TabIndex = 73;
            this.startTrackingButton.Text = "Start Tracking";
            this.startTrackingButton.UseVisualStyleBackColor = true;
            this.startTrackingButton.Click += new System.EventHandler(this.startTrackingButton_Click);
            // 
            // startSaveButton
            // 
            this.startSaveButton.Location = new System.Drawing.Point(535, 287);
            this.startSaveButton.Name = "startSaveButton";
            this.startSaveButton.Size = new System.Drawing.Size(87, 34);
            this.startSaveButton.TabIndex = 74;
            this.startSaveButton.Text = "Start Save";
            this.startSaveButton.UseVisualStyleBackColor = true;
            this.startSaveButton.Click += new System.EventHandler(this.startSaveButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(437, 287);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(92, 34);
            this.stopButton.TabIndex = 75;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 390);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.startSaveButton);
            this.Controls.Add(this.startTrackingButton);
            this.Controls.Add(this.saveDefaultParams);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.yScanBot);
            this.Controls.Add(this.xScanBot);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.yScanTop);
            this.Controls.Add(this.xScanTop);
            this.Controls.Add(this.yInitLabel);
            this.Controls.Add(this.xInitLabel);
            this.Controls.Add(this.yInit);
            this.Controls.Add(this.xInit);
            this.Controls.Add(this.yStartLabel);
            this.Controls.Add(this.xStartLabel);
            this.Controls.Add(this.yStart);
            this.Controls.Add(this.xStart);
            this.Controls.Add(this.clickCaliButton);
            this.Controls.Add(this.eyeThresholdLabel);
            this.Controls.Add(this.eyeThreshold);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.yREye);
            this.Controls.Add(this.xREye);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.yLEye);
            this.Controls.Add(this.xLEye);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.startCameraButton);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eyeThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yREye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xREye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yLEye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xLEye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yScanBot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xScanBot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yScanTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xScanTop)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startCameraButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label eyeThresholdLabel;
        private System.Windows.Forms.NumericUpDown eyeThreshold;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown yREye;
        private System.Windows.Forms.NumericUpDown xREye;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown yLEye;
        private System.Windows.Forms.NumericUpDown xLEye;
        private System.Windows.Forms.Button clickCaliButton;
        private System.Windows.Forms.Label yInitLabel;
        private System.Windows.Forms.Label xInitLabel;
        private System.Windows.Forms.NumericUpDown yInit;
        private System.Windows.Forms.NumericUpDown xInit;
        private System.Windows.Forms.Label yStartLabel;
        private System.Windows.Forms.Label xStartLabel;
        private System.Windows.Forms.NumericUpDown yStart;
        private System.Windows.Forms.NumericUpDown xStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown yScanBot;
        private System.Windows.Forms.NumericUpDown xScanBot;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown yScanTop;
        private System.Windows.Forms.NumericUpDown xScanTop;
        private System.Windows.Forms.Button saveDefaultParams;
        private System.Windows.Forms.Button startTrackingButton;
        private System.Windows.Forms.Button startSaveButton;
        private System.Windows.Forms.Button stopButton;
    }
}

